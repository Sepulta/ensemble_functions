"""
Test functions for the ensemble_functions:

TODO: getFromDict
TODO: more_or_less
TODO: query_dict
TODO: return_keys_bins_and_bincenters
TODO: return_float_list_of_dictkeys
TODO: generate_bins_from_keys
TODO: flatten_data_ensemble2d
TODO: flatten_data_ensemble1d
TODO: get_recursive_count
TODO: get_recursive_sum
TODO: get_recursive_sum_for_subkeys_1d
TODO: get_recursive_sum_for_subkeys_2d
TODO: merge_recursive_until_stop
TODO: recursively_merge_all_subdicts
TODO: merge_all_subdicts
TODO: merge_dicts
TODO: merge_and_flatten_two_levels
"""


# example_dict = {
#     'a': {
#         'b1': {
#             '1': {'c1': {'1': 1}},
#             '2': {'c2': {'2': 2}},
#         },
#         'b2': {
#             '1': {'c1': {'1': 3, '2': 4}},
#             '2': {'c3': {'3': 4}}
#         }
#     }
# }

# example_dict_2 = {
#     'a': {
#         '1': {
#             'b': {
#                 '1': 1,
#                 '2': 2,
#             }
#         },
#         '2': {
#             'b': {
#                 '2': 3,
#                 '3': 4,
#             }
#         }
#     }
# }

# example_dict_3 = {
#     'a': {
#         '1': {
#             'b': {
#                 '1': {
#                     'c': {
#                         '1': 1,
#                         '2': 2
#                     }
#                 },
#                 '2': {
#                     'c': {
#                         '2': 3,
#                         '3': 4,
#                     }
#                 },
#             }
#         },
#         '2': {
#             'b': {
#                 '2': {
#                     'c': {
#                         '3': 5,
#                         '4': 6,
#                     }
#                 },
#                 '3': {
#                     'c': {
#                         '4': 7,
#                         '5': 8
#                     }
#                 },
#             }
#         }
#     }
# }


# print(merge_all_subdicts(example_dict['a']))

# # Taking the recursive sum of all the subkeys. We could use this for plotting the value ranges for 
# print(get_recursive_sum_for_subkeys_1d(example_dict_2, 'a'))


# print(flatten_data_ensemble1d(get_recursive_sum_for_subkeys_1d(example_dict_2, 'a'), 'a'))

# # Taking the recursive_sum_for_subkeys_2d
# print(get_recursive_sum_for_subkeys_2d(example_dict_3, 'a', 'b'))


# print(flatten_data_ensemble2d(get_recursive_sum_for_subkeys_2d(example_dict_3, 'a', 'b'), 'a', 'b'))

# # Query example:
# res = query_dict(example_dict, 'c1', [1-1e-5, 2+1e-5])
# print(res)

# example_dict = {
#     'a': {
#         'b1': {
#             '1': {'c1': {'1': 1}},
#             '2': {'c2': {'2': 2}},
#         },
#         'b2': {
#             '1': {'c1': {'1': 3, '2': 4}},
#             '2': {'c3': {'3': 4}}
#         }
#     }
# }

# example_dict_2 = {
#     'a': {
#         '1': {
#             'b': {
#                 '1': 1,
#                 '2': 2,
#             }
#         },
#         '2': {
#             'b': {
#                 '2': 3,
#                 '3': 4,
#             }
#         }
#     }
# }

# example_dict_3 = {
#     'a': {
#         '1': {
#             'b': {
#                 '1': {
#                     'c': {
#                         '1': 1,
#                         '2': 2
#                     }
#                 },
#                 '2': {
#                     'c': {
#                         '2': 3,
#                         '3': 4,
#                     }
#                 },
#             }
#         },
#         '2': {
#             'b': {
#                 '2': {
#                     'c': {
#                         '3': 5,
#                         '4': 6,
#                     }
#                 },
#                 '3': {
#                     'c': {
#                         '4': 7,
#                         '5': 8
#                     }
#                 },
#             }
#         }
#     }
# }


# print(merge_all_subdicts(example_dict['a']))

# # Taking the recursive sum of all the subkeys. We could use this for plotting the value ranges for 
# print(get_recursive_sum_for_subkeys_1d(example_dict_2, 'a'))


# print(flatten_data_ensemble1d(get_recursive_sum_for_subkeys_1d(example_dict_2, 'a'), 'a'))

# # Taking the recursive_sum_for_subkeys_2d
# print(get_recursive_sum_for_subkeys_2d(example_dict_3, 'a', 'b'))


# print(flatten_data_ensemble2d(get_recursive_sum_for_subkeys_2d(example_dict_3, 'a', 'b'), 'a', 'b'))

# # Query example:
# res = query_dict(example_dict, 'c1', [1-1e-5, 2+1e-5])
# print(res)

# query_res_3 = query_dict(example_dict_3, 'b', more_or_less(2))
# print(query_res_3)

# print(get_recursive_sum_for_subkeys_2d(query_res_3, 'a', 'b'))


# print(flatten_data_ensemble2d(get_recursive_sum_for_subkeys_2d(query_res_3, 'a', 'b'), 'a', 'b'))

# example_dict_4 = {
#     'a': {
#         '1': {
#             'b': {
#                 '1': {
#                     'c': {
#                         '1': {'d': {'1': 2}},
#                         '2': {'d': {'2': 3}}
#                     }
#                 },
#                 '2': {
#                     'c': {
#                         '2': {'d': {'3': 4}},
#                         '3': {'d': {'4': 5}},
#                     }
#                 },
#             }
#         },
#         '2': {
#             'b': {
#                 '2': {
#                     'c': {
#                         '3': {'d': {'5': 6}},
#                         '4': {'d': {'6': 7}},
#                     }
#                 },
#                 '3': {
#                     'c': {
#                         '4': {'d': {'7': 8}},
#                         '5': {'d': {'8': 9}}
#                     }
#                 },
#             }
#         }
#     }
# }
