"""
Main function to run the tests for ensemble functions
"""

import unittest

if __name__ == "__main__":
    unittest.main()
